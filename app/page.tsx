"use client"

import { useState } from 'react';
import QRCode from 'react-qr-code'
import resolveConfig from 'tailwindcss/resolveConfig'
import config from "../tailwind.config"

export default function Home() {

  const DEFAULT_INPUT = "https://qr.liquid.pizza"

  const fullConfig = resolveConfig(config)
  const [content, setContent] = useState(DEFAULT_INPUT);

  const HL_COLOR = "#FB923C"

  return (
    <main className="grid place-items-center gap-y-20 p-4 h-full">
      <div className="col-span-1">
        <h1 className="text-xl">qr_generator</h1>
      </div>
      <div className="col-span">
        <input
          className="text-black p-2 rounded-md"
          value={content}
          onChange={e => setContent(e.target.value)}
          placeholder="Enter an URL or a text"
          type="search"
        />
      </div>
      <div className="col-span hover:scale-150">
        <QRCode
          value={content}
          viewBox={`0 0 256 256`}
          bgColor="#00000000"
          fgColor={HL_COLOR}
        />
      </div>
      <div className="col-span text-center">
        <h2>You entered </h2>
        <p className="truncate text-orange-400 max-w-md">
          <code className="">{content}</code>
        </p>
      </div>
    </main>
  )
}
